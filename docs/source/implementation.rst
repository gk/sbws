Implementation decissions
=========================

Dependencies
------------

When a needed feature is already implemented in some other software, there're
usually some things to consider whether to use that software as dependency or
re-implement the feature:

Possible advantages using other software:

- zero maintainance
- not reinventing the wheel

Possible disadvantages using other software:

- maybe too big
- maybe introduce security issues
- maybe is not maintaned

sbws version
````````````

Because some bwauths install sbws from the git repository, it is useful to
know from which git revision they install it from.
We'd prefer to do not see the git revision when it is installed from a git
tag or Debian package (which is usually built from a git tag or git archive
release).

A first solution would be to obtain the git revision at runtime, but:

- sbws is not usually running from the same directory as the git repository,
  as the installation might install it in other directory.
- if some other git repository is the current path, sbws might be obtaining
  the git revision of that other repository.

So next solution was to obtain the git revision at build/install time.
To achive this, an script should be call from the installer or at runtime
whenever `__version__` needs to be read.

While it could be implemented by us, there're two external tools that achive
this.

setuptools_scm
~~~~~~~~~~~~~~

https://github.com/pypa/setuptools_scm/

Advantages:

- does what we want, for 19 commits after 1.1.0 tag it'd add an string like
  `'1.1.1.dev19+g76ef2fe0.d20200221`.
  We don't need the date, but it can probably be removed and it does not hurt.
- we don't need to maintain it.

Disadvantages:

- it adds the extra dependency setuptools_scm.
- it does not obtain the version from a git archive, though there's other tool
  that does that.
- the version reported comes only from build time, so if we make a commit
  without running `setup.py`, sbws will not report the new version.

versioneer
~~~~~~~~~~

https://github.com/warner/python-versioneer

Advantages:

- it does not add any extra dependency. The first time, versioneer needs to
  be installed. When run, it will generate `versioneer.py` and `_version.py`,
  which are created from versioneer itself.  Then it can be uninstall
- does what we want, for 19 commits after 1.1.0 tag it'd add an string like
  `1.1.0+19.g76ef2fe0`. Note the difference with `1.1.0` from he `1.1.1`
  generated by
- we don't need to maintain it.
- it is also capable to obtain the version from a git archive.
- the version reported at build time and runtime is the same.

Disadvantages:

- it adds extra code to sbws and it's quite a lot
- the generated code is independent from the upstream and loses the tests.
- does not seem maintained.

Conclussion
~~~~~~~~~~~

Because `setuptools_scm` gives only the version at build time, we decided to
use `versioneer`.
We might need to change it in the future if starts giving problems with other
git or python versions or we find a way to make `setuptools_scm` to detect
the same version at buildtime and runtime.

See `<https://github.com/MartinThoma/MartinThoma.github.io/blob/1235fcdecda4d71b42fc07bfe7db327a27e7bcde/content/2018-11-13-python-package-versions.md>`_
for other comparative versioning python packages.